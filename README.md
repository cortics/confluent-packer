# confluent-packer
Packer build for Confluent Platform - https://www.confluent.io/

Download Packer from https://www.packer.io/downloads.html and add to PATH. 

Clone the repo and then run  `packer build <file.json>`. Should take 5-6 min to complete.
